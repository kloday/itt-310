/*Program to search for words within a text file using search strings*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Windows.h>


// File path to search for named files
// This path and file name will change from user to user on each workstation
#define FILE_DIR "C:\\Users\\theod\\OneDrive\\Documents\\GCU\\Sample_file\\Project_Proposal\\sensitive_information.txt"

char c; // Global variable
FILE* ptr_to_file; // Global pointer
get_the_char(); // Global function

int main(int argc, char* argv[])
{
	/*Structure for strings*/
	struct strings {
		char word[20]; // Fixed buffers for amount of characters allowed
		char text[1000]; 
	};

	/*Assignment of strings callouts*/
	struct strings string1;
	struct strings string2;

	int i; // Character integer for search string
	
	char* search_for_string = string1.word;// Pointer to user input
								
	
		printf("Search for: "); // Display "Search for: " to console
		scanf_s("%s", string1.word, (unsigned)_countof(string1.word)); // User inputs word to search within .txt file

	int len = strlen(search_for_string); // Variable to compare string lengths

	fopen_s(&ptr_to_file, FILE_DIR, "r"); // Open and read file

	/*Color input for highlighting text after string found.*/
	int oldColor = 15;
	CONSOLE_SCREEN_BUFFER_INFO csbi = { 0 };
	if (GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi))
	{
		oldColor = csbi.wAttributes;
	}

	/*Scan characters*/
	while (!feof(ptr_to_file))
	{
		get_the_char();
		i = 0;
	
		if (c == search_for_string[0]) // Characters matching input string
		{
			/*do Loop evaluates the body of text to be read for sting match*/
			do
			{
				string2.text[i++] = c; // Integer to increment each character of text
				c = fgetc(ptr_to_file);
			}
			/*while Loop to determine the output*/
			while (!feof(ptr_to_file) && i < len && i < 1000);
			string2.text[i] = '\0'; 

			/*Compares search string with character strings in file, colors matched strings red*/
			if (0 == strcmp(string2.text, search_for_string))
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 206);
				printf("%s", string2.text);
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), oldColor);
			}
			else
				printf("%s", string2.text); // Returns characters that do not match
		
		}
		/*If no match is found, console returns "*****NO MATCH FOUND*****" highlighted red*/
		if (search_for_string != 0, feof(ptr_to_file))
		{
			string2.text[0] = '\0';
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 206);
			printf("\n\n*****NO MATCH FOUND*****");
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), oldColor);
			get_the_char();
		}
		printf("%c", c); //Print all characters in .txt file
	}
	// Close text file
	fclose(ptr_to_file);
	return 0;
}

get_the_char()
{
	c = fgetc(ptr_to_file);

	if (feof(ptr_to_file))// End of file indicator, exit from file executed
	{
		printf("\n\nEND OF FILE\n");// Displayed upon exiting

		exit(EXIT_FAILURE);
	}
		

}